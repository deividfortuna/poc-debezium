package com.spm.pocdebezium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocDebeziumApplication {
    // https://vladmihalcea.com/a-beginners-guide-to-cdc-change-data-capture/
    public static void main(String[] args) {
        SpringApplication.run(PocDebeziumApplication.class, args);
    }
}
