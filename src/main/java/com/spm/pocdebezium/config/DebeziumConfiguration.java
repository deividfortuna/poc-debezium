package com.spm.pocdebezium.config;

import io.debezium.connector.postgresql.PostgresConnectorConfig;
import io.debezium.embedded.EmbeddedEngine;
import io.debezium.util.Clock;
import org.apache.kafka.connect.source.SourceRecord;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class DebeziumConfiguration {
    public DebeziumConfiguration() {

        // https://docs.confluent.io/current/connect/debezium-connect-postgres/quickstart.html
        io.debezium.config.Configuration config = io.debezium.config.Configuration.create()
                .with(EmbeddedEngine.CONNECTOR_CLASS, "io.debezium.connector.postgresql.PostgresConnector")
                .with(EmbeddedEngine.ENGINE_NAME, "poc")
                .with(PostgresConnectorConfig.SERVER_NAME, "poc")
                .with(PostgresConnectorConfig.DATABASE_NAME, "poc")
                .with(PostgresConnectorConfig.HOSTNAME, "localhost")
                .with(PostgresConnectorConfig.PORT, 5432)
                .with(PostgresConnectorConfig.USER, "poc")
                .with(PostgresConnectorConfig.PASSWORD, "poc")
                .build();

        EmbeddedEngine engine = EmbeddedEngine.create()
                .using(config)
                .using(this.getClass().getClassLoader())
                .using(Clock.SYSTEM)
                .notifying(this::sendRecord)
                .build();

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(engine);
    }

    public void sendRecord(SourceRecord sourceRecord) {
        // Publish the event to AWS if necessary
        System.out.println(sourceRecord.toString());
    }
}
